﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using Xunit;

namespace SortingTests
{
	public static class AssertOrdering
	{
        public static void IsSetSorted(DataSet set, int expectedSize)
        {
            Assert.NotNull(set);
            Assert.Equal(expectedSize, set.Count);

            int value = set[0];
            for (int i = 1; i < set.Count; ++i)
            {
                if (value > set[i])
                {
                    Assert.Equal(string.Format("{0} <= {1} at {2}", value, set[i], i),
                        string.Format("{0} > {1} at {2}", value, set[i], i));
                }
                value = set[i];
            }
        }
	}
}
