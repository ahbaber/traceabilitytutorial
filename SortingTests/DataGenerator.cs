﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using Sorting.Factories;
using System;
using System.Linq;

namespace SortingTests
{
    public class DataGenerator
    {
        private static Random _random = new Random();
		private readonly DataSetFactory _factory;

		public DataGenerator(DataSetFactory factory = null)
		{
			_factory = factory ?? new DataSetFactory();
		}

		public DataSet CreateRandom(int size)
        {
            int max = size * 2;
            var data = Enumerable.Range(0, size).Select(n => _random.Next(max)).ToList<int>();
			return _factory.Construct(data);
        }

        public DataSet CreateOrdered(int size)
        {
            return _factory.Construct(Enumerable.Range(1, size).ToList<int>());
        }

        public DataSet CreateReveresed(int size)
        {
            var data = Enumerable.Range(1, size).ToList<int>();
            data.Reverse();
            return _factory.Construct(data);
        }

        public DataSet CreateRepeated(int size)
        {
            return _factory.Construct(Enumerable.Repeat(42, size).ToList<int>());
        }

        public DataSet CreateQuickSortWorstCase(int size)
        {
            int initial = size % 2 == 0
                ? 1
                : 2;
            var data = Enumerable.Repeat(initial, size).ToList<int>();
            int value = data.Count - 1;
            int middle = (data.Count - 1) / 2;
            data[0] = value;

            for (int i = middle; i > 0; --i, value-=2)
            { data[i] = value; }

            value = data.Count - 3;
            int start = data.Count - 1;
            int step = 2;
            int offset = 1;

            while (value > 0)
            {
                for (int i = start; i > middle; i-=step, value-=2)
                { data[i] = value; }
                start -= offset;
                offset *= 2;
                step *= 2;
            }

            return _factory.Construct(data);
        }
    }
}
