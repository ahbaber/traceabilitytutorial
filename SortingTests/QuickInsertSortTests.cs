﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Factories;
using Xunit;

namespace SortingTests
{
	public class QuickInsertSortTests
	{
		private static readonly DataGenerator _staticDataGenerator = new DataGenerator();
		private static readonly QuickInsertSortFactory _staticQuickInsertSortFactory = new QuickInsertSortFactory(new InsertSortFactory());

		private readonly DataGenerator _dataGenerator;
		private readonly QuickInsertSortFactory _quickInsertSortFactory;

		public QuickInsertSortTests(QuickInsertSortFactory quickInsertSortFactory = null, DataGenerator dataGenerator = null)
		{
			_quickInsertSortFactory = quickInsertSortFactory ?? _staticQuickInsertSortFactory;
			_dataGenerator = dataGenerator ?? _staticDataGenerator;
		}

		[Fact]
		public void Sort_OrderedData_DataIsSorted()
		{
			var set = _dataGenerator.CreateOrdered(32);
			var sorter = _quickInsertSortFactory.Construct();
			sorter.Sort(set);
			AssertOrdering.IsSetSorted(set, 32);
		}

		[Fact]
		public void Sort_ReveresedData_DataIsSorted()
		{
			var set = _dataGenerator.CreateReveresed(32);
			var sorter = _quickInsertSortFactory.Construct();
			sorter.Sort(set);
			AssertOrdering.IsSetSorted(set, 32);
		}

		[Fact]
		public void Sort_RandomData_DataIsSorted()
		{
			var set = _dataGenerator.CreateRandom(32);
			var sorter = _quickInsertSortFactory.Construct();
			sorter.Sort(set);
			AssertOrdering.IsSetSorted(set, 32);
		}

		[Fact]
		public void Sort_RepeatedData_DataIsSorted()
		{
			var set = _dataGenerator.CreateRepeated(32);
			var sorter = _quickInsertSortFactory.Construct();
			sorter.Sort(set);
			AssertOrdering.IsSetSorted(set, 32);
		}

		[Fact]
		public void Sort_QuickSortWorstCaseData_DataIsSorted()
		{
			var set = _dataGenerator.CreateQuickSortWorstCase(32);
			var sorter = _quickInsertSortFactory.Construct();
			sorter.Sort(set);
			AssertOrdering.IsSetSorted(set, 32);
		}
	}
}
