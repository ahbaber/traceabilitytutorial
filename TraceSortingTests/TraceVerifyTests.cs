﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using Sorting.Factories;
using TraceSorting.Algorithms;
using TraceSorting.Factories;
using TraceCodeGenerator;
using Xunit;

namespace TraceSortingTests
{
	public class TraceVerifyTests
	{
		[Fact]
		public void Verify_Set()
		{
			var results = Verify.VerifyTraceInheritence(typeof(DataSet), typeof(TraceDataSet));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_SetFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(DataSetFactory), typeof(TraceDataSetFactory));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Algorithms_SimpleSort()
		{
			var results = Verify.VerifyTraceInheritence(typeof(SimpleSort), typeof(TraceSimpleSort));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_SimpleSortFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(SimpleSortFactory), typeof(TraceSimpleSortFactory));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Algorithms_SelectionSort()
		{
			var results = Verify.VerifyTraceInheritence(typeof(SelectionSort), typeof(TraceSelectionSort));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_SelectionSortFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(SelectionSortFactory), typeof(TraceSelectionSortFactory));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Algorithms_InsertSort()
		{
			var results = Verify.VerifyTraceInheritence(typeof(InsertSort), typeof(TraceInsertSort));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_InsertSortFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(InsertSortFactory), typeof(TraceInsertSortFactory));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Algorithms_QuickSort()
		{
			var results = Verify.VerifyTraceInheritence(typeof(QuickSort), typeof(TraceQuickSort));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_QuickSortFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(QuickSortFactory), typeof(TraceQuickSortFactory));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Algorithms_QuickInsertSort()
		{
			var ignore = new SkipMap().Add("MinSegment", false);
			var results = Verify.VerifyTraceInheritence(typeof(QuickInsertSort), typeof(TraceQuickInsertSort), ignore);
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_QuickInsertSortFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(QuickInsertSortFactory), typeof(TraceQuickInsertSortFactory));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Algorithms_HeapSort()
		{
			var results = Verify.VerifyTraceInheritence(typeof(HeapSort), typeof(TraceHeapSort));
			Assert.Empty(results.Errors);
		}

		[Fact]
		public void Verify_Factories_HeapSortFactory()
		{
			var results = Verify.VerifyTraceInheritence(typeof(HeapSortFactory), typeof(TraceHeapSortFactory));
			Assert.Empty(results.Errors);
		}
	}
}
