﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using SortingTests;
using Traceability;
using TraceSorting.Factories;

namespace TraceSortingTests
{
	public class TraceQuickInsertSortTests : QuickInsertSortTests
	{
		private static readonly FakeTracer _tracer = new FakeTracer();
		private static readonly DataGenerator _dataGenerator = new DataGenerator(new TraceDataSetFactory(_tracer));
		private static readonly TraceInsertSortFactory _insertSortFactory = new TraceInsertSortFactory(_tracer);
		private static readonly TraceQuickInsertSortFactory _quickInsertSortFactory = new TraceQuickInsertSortFactory(_tracer, _insertSortFactory);

		public TraceQuickInsertSortTests()
		:base(_quickInsertSortFactory, _dataGenerator)
		{ }
	}
}
