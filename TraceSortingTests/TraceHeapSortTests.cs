﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using SortingTests;
using Traceability;
using TraceSorting.Factories;

namespace TraceSortingTests
{
	public class TraceHeapSortTests : HeapSortTests
	{
		private static readonly FakeTracer _tracer = new FakeTracer();
		private static readonly DataGenerator _dataGenerator = new DataGenerator(new TraceDataSetFactory(_tracer));
		private static readonly TraceHeapSortFactory _sortFactory = new TraceHeapSortFactory(_tracer);

		public TraceHeapSortTests()
		: base(_sortFactory, _dataGenerator)
		{ }
	}
}
