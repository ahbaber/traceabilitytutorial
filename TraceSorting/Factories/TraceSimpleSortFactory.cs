// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using Sorting.Factories;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
	public class TraceSimpleSortFactory : Sorting.Factories.SimpleSortFactory
	{
		public TraceSimpleSortFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override SimpleSort Construct()
		{
			return new TraceSimpleSort(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
