// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using Sorting.Factories;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
	public class TraceQuickSortFactory : Sorting.Factories.QuickSortFactory
	{
		public TraceQuickSortFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override QuickSort Construct()
		{
			return new TraceQuickSort(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
