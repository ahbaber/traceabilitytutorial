// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using Sorting.Factories;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
	public class TraceSelectionSortFactory : Sorting.Factories.SelectionSortFactory
	{
		public TraceSelectionSortFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override SelectionSort Construct()
		{
			return new TraceSelectionSort(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
