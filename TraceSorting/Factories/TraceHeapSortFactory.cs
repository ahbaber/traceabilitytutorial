// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using Sorting.Factories;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
	public class TraceHeapSortFactory : Sorting.Factories.HeapSortFactory
	{
		public TraceHeapSortFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override HeapSort Construct()
		{
			return new TraceHeapSort(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
