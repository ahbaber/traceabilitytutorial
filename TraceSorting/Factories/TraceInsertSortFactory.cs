// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using Sorting.Factories;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
	public class TraceInsertSortFactory : Sorting.Factories.InsertSortFactory
	{
		public TraceInsertSortFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override InsertSort Construct()
		{
			return new TraceInsertSort(_tracer);
		}

		private readonly ITracer _tracer;
	}
}
