// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using Sorting.Factories;
using System;
using System.Collections.Generic;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
	public class TraceDataSetFactory : Sorting.Factories.DataSetFactory
	{
		public TraceDataSetFactory(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override DataSet Construct()
		{
			return new TraceDataSet(_tracer);
		}

		public override DataSet Construct(List<int> list)
		{
			return new TraceDataSet(_tracer, list);
		}

		public override DataSet Construct(DataSet set)
		{
			return new TraceDataSet(_tracer, set);
		}

		private readonly ITracer _tracer;
	}
}
