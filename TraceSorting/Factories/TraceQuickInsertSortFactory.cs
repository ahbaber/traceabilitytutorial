﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using Traceability;
using TraceSorting.Algorithms;

namespace TraceSorting.Factories
{
    public class TraceQuickInsertSortFactory : Sorting.Factories.QuickInsertSortFactory
    {
        private readonly ITracer _tracer;

        public TraceQuickInsertSortFactory(ITracer tracer, TraceInsertSortFactory insert)
        :base(insert)
        {
            _tracer = tracer;
        }

        public override QuickInsertSort Construct()
        {
            return new TraceQuickInsertSort(_tracer, _insertSortFactory.Construct());
        }
    }
}
