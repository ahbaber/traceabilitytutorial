// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;
using System.Collections.Generic;
using Traceability;

namespace TraceSorting.Algorithms
{
	public class TraceDataSet : Sorting.Algorithms.DataSet
	{
		public TraceDataSet(ITracer tracer)
		{
			_tracer = tracer;
		}

		public TraceDataSet(ITracer tracer, List<int> list)
		:base(list)
		{
			_tracer = tracer;
		}

		public TraceDataSet(ITracer tracer, DataSet set)
		:base(set)
		{
			_tracer = tracer;
		}

		public override int this[int i]
		{
			get { return _tracer.GetIndexer("DataSet", i, base[i]); }
			set { base[i] = _tracer.SetIndexer("DataSet", i, value); }
		}

		public override int Count
		{
			get { return _tracer.GetProperty("DataSet.Count", base.Count); }
		}

		public override DataSet Swap(int i, int j)
		{
			_tracer.FunctionStart("DataSet.Swap");
			_tracer.Parameter("i", i);
			_tracer.Parameter("j", j);
			var result = base.Swap(i, j);
			return _tracer.FunctionEnd(result);
		}

		public override DataSet Copy(List<int> list)
		{
			_tracer.FunctionStart("DataSet.Copy");
			_tracer.Parameter("list", list);
			var result = base.Copy(list);
			return _tracer.FunctionEnd(result);
		}

		public override DataSet Copy(DataSet set)
		{
			_tracer.FunctionStart("DataSet.Copy");
			_tracer.Parameter("set", set);
			var result = base.Copy(set);
			return _tracer.FunctionEnd(result);
		}

		public override void PreCheckData()
		{
			_tracer.FunctionStart("DataSet.PreCheckData");
			base.PreCheckData();
			_tracer.FunctionEnd();
		}

		public override int ResponseData()
		{
			_tracer.FunctionStart("DataSet.ResponseData");
			var result = base.ResponseData();
			return _tracer.FunctionEnd(result);
		}

		private readonly ITracer _tracer;
	}
}
