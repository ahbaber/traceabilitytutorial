// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;
using Traceability;

namespace TraceSorting.Algorithms
{
	public class TraceSelectionSort : Sorting.Algorithms.SelectionSort
	{
		public TraceSelectionSort(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override void Sort(DataSet set)
		{
			_tracer.FunctionStart("SelectionSort.Sort");
			_tracer.Parameter("set", set);
			base.Sort(set);
			_tracer.FunctionEnd();
		}

		protected override int InnerLoop(DataSet set, int i)
		{
			_tracer.FunctionStart("SelectionSort.InnerLoop");
			_tracer.Parameter("set", set);
			_tracer.Parameter("i", i);
			var result = base.InnerLoop(set, i);
			return _tracer.FunctionEnd(result);
		}

		private readonly ITracer _tracer;
	}
}
