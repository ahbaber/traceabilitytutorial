// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;
using Traceability;

namespace TraceSorting.Algorithms
{
	public class TraceQuickInsertSort : Sorting.Algorithms.QuickInsertSort
	{
		public TraceQuickInsertSort(ITracer tracer, InsertSort insertSort)
		:base(insertSort)
		{
			_tracer = tracer;
		}

		// skipped NotVirtual int MinSegment

		public override void Sort(DataSet set)
		{
			_tracer.FunctionStart("QuickInsertSort.Sort");
			_tracer.Parameter("set", set);
			base.Sort(set);
			_tracer.FunctionEnd();
		}

		protected override void Sort(DataSet set, int start, int end)
		{
			_tracer.FunctionStart("QuickInsertSort.Sort");
			_tracer.Parameter("set", set);
			_tracer.Parameter("start", start);
			_tracer.Parameter("end", end);
			base.Sort(set, start, end);
			_tracer.FunctionEnd();
		}

		protected override int GetMedianValue(int i, int j, int k)
		{
			_tracer.FunctionStart("QuickInsertSort.GetMedianValue");
			_tracer.Parameter("i", i);
			_tracer.Parameter("j", j);
			_tracer.Parameter("k", k);
			var result = base.GetMedianValue(i, j, k);
			return _tracer.FunctionEnd(result);
		}

		protected override int Partition(DataSet set, int start, int end)
		{
			_tracer.FunctionStart("QuickInsertSort.Partition");
			_tracer.Parameter("set", set);
			_tracer.Parameter("start", start);
			_tracer.Parameter("end", end);
			var result = base.Partition(set, start, end);
			return _tracer.FunctionEnd(result);
		}

		private readonly ITracer _tracer;
	}
}
