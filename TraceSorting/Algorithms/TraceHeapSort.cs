// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;
using Traceability;

namespace TraceSorting.Algorithms
{
	public class TraceHeapSort : Sorting.Algorithms.HeapSort
	{
		public TraceHeapSort(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override void Sort(DataSet set)
		{
			_tracer.FunctionStart("HeapSort.Sort");
			_tracer.Parameter("set", set);
			base.Sort(set);
			_tracer.FunctionEnd();
		}

		protected override void MaxHeapUp(DataSet set, int i)
		{
			_tracer.FunctionStart("HeapSort.MaxHeapUp");
			_tracer.Parameter("set", set);
			_tracer.Parameter("i", i);
			base.MaxHeapUp(set, i);
			_tracer.FunctionEnd();
		}

		protected override void SiftDown(DataSet set, int i, int last)
		{
			_tracer.FunctionStart("HeapSort.SiftDown");
			_tracer.Parameter("set", set);
			_tracer.Parameter("i", i);
			_tracer.Parameter("last", last);
			base.SiftDown(set, i, last);
			_tracer.FunctionEnd();
		}

		protected override void CheckChild(DataSet set, int i, int child, int last)
		{
			_tracer.FunctionStart("HeapSort.CheckChild");
			_tracer.Parameter("set", set);
			_tracer.Parameter("i", i);
			_tracer.Parameter("child", child);
			_tracer.Parameter("last", last);
			base.CheckChild(set, i, child, last);
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
