// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;
using Traceability;

namespace TraceSorting.Algorithms
{
	public class TraceInsertSort : Sorting.Algorithms.InsertSort
	{
		public TraceInsertSort(ITracer tracer)
		{
			_tracer = tracer;
		}

		public override void Sort(DataSet set)
		{
			_tracer.FunctionStart("InsertSort.Sort");
			_tracer.Parameter("set", set);
			base.Sort(set);
			_tracer.FunctionEnd();
		}

		protected override void InnerLoop(DataSet set, int i)
		{
			_tracer.FunctionStart("InsertSort.InnerLoop");
			_tracer.Parameter("set", set);
			_tracer.Parameter("i", i);
			base.InnerLoop(set, i);
			_tracer.FunctionEnd();
		}

		private readonly ITracer _tracer;
	}
}
