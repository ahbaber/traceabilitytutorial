﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using Sorting.Algorithms;
using System;
using TraceCodeGenerator;

namespace CodeGeneration
{
	class Program
	{
		static void Main(string[] args)
		{
			GenerateClasses(typeof(DataSet));
			GenerateClasses(typeof(SimpleSort));
			GenerateClasses(typeof(SelectionSort));
			GenerateClasses(typeof(InsertSort));
			GenerateClasses(typeof(QuickSort));
			GenerateClasses(typeof(HeapSort));

			//QuickInsertSortFactory was manually generated
			var trace = new TraceClassGenerator(typeof(QuickInsertSort), "TraceSorting.Algorithms");
			WriteFile(trace);
			// TraceQuickInsertSortFactory was manually generated

			Console.WriteLine("Done");
		}

		private static void GenerateClasses(Type type)
		{
			var factory = new FactoryGenerator(type, "Sorting.Factories");
			WriteFile(factory);

			var trace = new TraceClassGenerator(type, "TraceSorting.Algorithms");
			WriteFile(trace);

			var traceFactory = new TraceFactoryGenerator(factory, trace, "TraceSorting.Factories");
			WriteFile(traceFactory);
		}

		private static void WriteFile(IClassGenerator classGen)
		{
			Console.WriteLine(_fileWriter.CreateFile(classGen));
		}

		private static FileWriter _fileWriter = new FileWriter();
	}
}
