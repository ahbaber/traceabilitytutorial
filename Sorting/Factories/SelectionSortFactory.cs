// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;

namespace Sorting.Factories
{
	public class SelectionSortFactory
	{
		public virtual SelectionSort Construct()
		{
			return new SelectionSort();
		}
	}
}
