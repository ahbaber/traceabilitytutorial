// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;

namespace Sorting.Factories
{
	public class HeapSortFactory
	{
		public virtual HeapSort Construct()
		{
			return new HeapSort();
		}
	}
}
