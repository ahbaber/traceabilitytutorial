// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;
using System.Collections.Generic;

namespace Sorting.Factories
{
	public class DataSetFactory
	{
		public virtual DataSet Construct()
		{
			return new DataSet();
		}

		public virtual DataSet Construct(List<int> list)
		{
			return new DataSet(list);
		}

		public virtual DataSet Construct(DataSet set)
		{
			return new DataSet(set);
		}
	}
}
