﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

//Notes:
//  This factory was manually written since it uses another factory to construct
//  a QuickInsortSort instance.  This gives us two advantages:
//  1) The InsertSort intstance is automatically created for us.
//  2) We can change the InsertSortFactory we use if we want to.

//  If the InsertSortFactory needed to use arguments in its Construct(), then
//  we would need to have those same arguments in QuickInsertSortFactory Construct()
//  as well.

using Sorting.Algorithms;

namespace Sorting.Factories
{
    public class QuickInsertSortFactory
    {
        protected InsertSortFactory _insertSortFactory;

        public QuickInsertSortFactory(InsertSortFactory insertSortFactory)
        {
            _insertSortFactory = insertSortFactory;
        }

        public virtual QuickInsertSort Construct()
        {
            return new QuickInsertSort(_insertSortFactory.Construct());
        }
    }
}
