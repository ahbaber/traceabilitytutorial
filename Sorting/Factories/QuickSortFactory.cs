// Auto generated file
// Any modification to this file may be lost
using Sorting.Algorithms;
using System;

namespace Sorting.Factories
{
	public class QuickSortFactory
	{
		public virtual QuickSort Construct()
		{
			return new QuickSort();
		}
	}
}
