﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Sorting.Algorithms
{
    public interface ISorter
    {
        void Sort(DataSet set);
    }
}
