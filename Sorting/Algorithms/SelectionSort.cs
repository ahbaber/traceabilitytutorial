﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Sorting.Algorithms
{
    public class SelectionSort : ISorter
    {
        public virtual void Sort(DataSet set)
        {
            for (int i = 0; i < set.Count-1; ++i)
            {
                int smallest = InnerLoop(set, i);
                if (smallest != i)
                {
                    set.Swap(i, smallest);
                }
            }
        }

        protected virtual int InnerLoop(DataSet set, int i)
        {
            int smallest = i;
            for (int j = i + 1; j < set.Count; ++j)
            {
                if (set[j] < set[smallest])
                {
                    smallest = j;
                }
            }
            return smallest;
        }
    }
}
