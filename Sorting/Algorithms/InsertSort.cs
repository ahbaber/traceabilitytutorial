﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Sorting.Algorithms
{
    public class InsertSort : ISorter
    {
        public virtual void Sort(DataSet set)
        {
            for (int i = 1; i < set.Count; ++i)
                InnerLoop(set, i);
        }

        protected virtual void InnerLoop(DataSet set, int i)
        {
            while (i > 0 && set[i] < set[i-1])
            {
                set.Swap(i - 1, i);
                --i;
            }
        }
    }
}
