﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System.Collections.Generic;

namespace Sorting.Algorithms
{
	public class DataSet
	{
		public virtual int this[int i] { get { return _data[i]; } set { _data[i] = value; } }
		public virtual int Count { get { return _data.Count; } }

		public DataSet()
		{
			_data = new List<int>();
		}

		public DataSet(List<int> list)
		{
			_data = list;
		}

		public DataSet(DataSet set)
		{
			_data = new List<int>(set._data);
		}

		public virtual DataSet Swap(int i, int j)
		{
			var temp = _data[i];
			_data[i] = _data[j];
			_data[j] = temp;
			return this;
		}

		public virtual DataSet Copy(List<int> list)
		{
			_data.Clear();
			foreach (int n in list)
			{
				_data.Add(n);
			}
			return this;
		}

		public virtual DataSet Copy(DataSet set)
		{
			_data.Clear();
			foreach (int n in set._data)
			{
				_data.Add(n);
			}
			return this;
		}

		public virtual void PreCheckData()
		{
			int dummy = 0;
			for (int i = 0; i < _data.Count; ++i)
			{
				dummy += this[i];
			}
		}

		public virtual int ResponseData()
		{
			if (_data == null || _data.Count == 0)
			{
				return 0;
			}

			int outOfPlace = 0;
			int prevValue = this[0];
			for (int i = 1; i < _data.Count; ++i)
			{
				int current = this[i];
				if (current < prevValue)
				{
					++outOfPlace;
				}
				prevValue = current;
			}
			return outOfPlace;
		}

		private readonly List<int> _data;
	}
}
