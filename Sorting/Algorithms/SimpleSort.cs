﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Sorting.Algorithms
{
    public class SimpleSort : ISorter
    {
        public virtual void Sort(DataSet set)
        {
            for (int i = 0; i < set.Count - 1; ++i)
                InnerLoop(set, i);
        }

        protected virtual void InnerLoop(DataSet set, int i)
        {
            for (int j = i + 1; j < set.Count; ++j)
            {
                if (set[j] < set[i])
                    set.Swap(i, j);
            }
        }
    }
}
