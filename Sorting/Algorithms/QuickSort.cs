﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details
using System;

namespace Sorting.Algorithms
{
    public class QuickSort : ISorter
    {
        public virtual void Sort(DataSet set)
        {
            Sort(set, 0, set.Count-1);
        }

        protected virtual void Sort(DataSet set, int start, int end)
        {
            if (start < end)
            {
                int pivot = Partition(set, start, end);
                {
                    Sort(set, start, pivot);
                    Sort(set, pivot+1, end);
                }
            }
        }

        protected virtual int GetMedianValue(int i, int j, int k)
        {
            return Math.Max(Math.Min(i, j), Math.Min(Math.Max(i, j), k));
        }
        
        // Hoare partition scheme
        // https://en.wikipedia.org/wiki/Quicksort
        protected virtual int Partition(DataSet set, int start, int end)
        {
            int median = (start + end) / 2;
            median = GetMedianValue(set[start], set[median], set[end]);

            --start;
            ++end;
            while (true)
            {
                do
                {
                    ++start;
                }
                while (set[start] < median);
                do
                {
                    --end;
                }
                while (set[end] > median);
                
                if (start >= end)
                {
                    return end;
                }
                set.Swap(start, end);
            }
        }
    }
}
