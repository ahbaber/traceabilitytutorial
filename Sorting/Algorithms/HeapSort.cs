﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Sorting.Algorithms
{
    public class HeapSort : ISorter
    {
        public virtual void Sort(DataSet set)
        {
            for (int i = 1; i < set.Count; ++i)
            {
                MaxHeapUp(set, i);
            }
            int last = set.Count - 1;
            while (last > 0)
            {
                set.Swap(0, last);
                --last;
                SiftDown(set, 0, last);
            }
        }

        protected virtual void MaxHeapUp(DataSet set, int i)
        {
            if (i > 0)
            {
                int parent = (i - 1) / 2;
                if (set[i] > set[parent])
                {
                    set.Swap(parent, i);
                    MaxHeapUp(set, parent);
                }
            }
        }

        protected virtual void SiftDown(DataSet set, int i, int last)
        {
            int child1 = i * 2 + 1;
            if (child1 > last)
                return;
            int child2 = child1 + 1;
            if (child2 > last)
            {
                CheckChild(set, i, child1, last);
                return;
            }
            if (set[child1] > set[child2])
            {
                CheckChild(set, i, child1, last);
            }
            else
                CheckChild(set, i, child2, last);
        }

        protected virtual void CheckChild(DataSet set, int i, int child, int last)
        {
            if (set[i] < set[child])
            {
                set.Swap(i, child);
                SiftDown(set, child, last);
            }
        }
    }
}