﻿//Copyright (c) 2018,2019 Adin Hunter Baber
//Under the MIT License
//See License.txt for more details

namespace Sorting.Algorithms
{
	public class QuickInsertSort : QuickSort
	{
		private InsertSort _insertSort;
		public int MinSegment { get; set; } = 8;

		public QuickInsertSort(InsertSort insertSort)
		{
			_insertSort = insertSort;
		}

		public override void Sort(DataSet set)
		{
			Sort(set, 0, set.Count - 1);
			_insertSort.Sort(set);
		}

		protected override void Sort(DataSet set, int start, int end)
		{
			if (start + MinSegment < end)
			{
				int pivot = Partition(set, start, end);
				{
					Sort(set, start, pivot);
					Sort(set, pivot + 1, end);
				}
			}
		}
	}
}
